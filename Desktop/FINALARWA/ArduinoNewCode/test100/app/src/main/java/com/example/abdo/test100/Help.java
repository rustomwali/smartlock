package com.example.abdo.test100;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashSet;
import java.util.Set;

public class Help extends AppCompatActivity {

    Button call , save ,call2 ,call3;
    public SharedPreferences prefrence;
    public SharedPreferences.Editor editor;
    public String Num="";
    TextView text1;
    TextView text2;
    TextView text3;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        final   EditText number3=(EditText) findViewById(R.id.number3);
        final   EditText number2=(EditText) findViewById(R.id.number2);
        final   EditText number=(EditText) findViewById(R.id.number);




            call = findViewById(R.id.call);
            call2 = findViewById(R.id.call2);
            call3 = findViewById(R.id.call3);
            save = findViewById(R.id.save);
             text1 = findViewById(R.id.numberText);
             text2 = findViewById(R.id.number2Text);
             text3 = findViewById(R.id.number3Text);
            prefrence = getSharedPreferences("prefence",MODE_PRIVATE);
            editor=prefrence.edit();

            if(prefrence.getString("number3", null)!=null){
                text3.setVisibility(View.VISIBLE);
                number3.setVisibility(View.GONE);
                text3.setText(prefrence.getString("number3", null));
                save.setVisibility(View.GONE);

            }

        if(prefrence.getString("number2", null)!=null){
            text2.setVisibility(View.VISIBLE);
            number2.setVisibility(View.GONE);
            text2.setText(prefrence.getString("number2", null));
            save.setVisibility(View.GONE);

        }

        if(prefrence.getString("number", null)!=null){
            text1.setVisibility(View.VISIBLE);
            number.setVisibility(View.GONE);
            text1.setText(prefrence.getString("number", null));
            save.setVisibility(View.GONE);

        }
            save.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    //et entered data
                    editor.putString("number2",  number2.getText().toString()).apply();
                    editor.putString("number",  number.getText().toString()).apply();
                    editor.putString("number3",  number3.getText().toString()).apply();

                    text1.setVisibility(View.VISIBLE);
                    number.setVisibility(View.GONE);
                    text1.setText(prefrence.getString("number", null));


                    text2.setVisibility(View.VISIBLE);
                    number2.setVisibility(View.GONE);
                    text2.setText(prefrence.getString("number2", null));



                    text3.setVisibility(View.VISIBLE);
                    number3.setVisibility(View.GONE);
                    text3.setText(prefrence.getString("number3", null));
                    save.setVisibility(View.GONE);




                }
            });

        call3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Num=  prefrence.getString("number", String.valueOf(number));
                dialContactPhone(Num);


            }
        });

        call2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Num=  prefrence.getString("number2", String.valueOf(number2));
                dialContactPhone(Num);


            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                Num=  prefrence.getString("number3", String.valueOf(number3));
                dialContactPhone(Num);


            }
        });

    }

    private void dialContactPhone(final String phoneNumber) {
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null)));
    }
}
