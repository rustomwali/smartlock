package com.example.abdo.test100;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class History extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {

    private SharedPreferences prefrence;
    private MyRecyclerViewAdapter adapter;
    private MyRecyclerViewAdapter adapterClosed;
    private ArrayList<String> timeAndDateOpen;
    private ArrayList<String> timeAndDateClosed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        timeAndDateOpen = new ArrayList<String>();
        timeAndDateClosed = new ArrayList<String>();
        prefrence = getSharedPreferences("prefence", MODE_PRIVATE);
        HashSet<String> storedTimeAndDateOpen = (HashSet<String>) prefrence.getStringSet("TimeAndDateOpen", null);
        String value = "";
        if (storedTimeAndDateOpen != null){
            Iterator iterator = storedTimeAndDateOpen.iterator();
            while (iterator.hasNext()) {
                value = (String) iterator.next();
                timeAndDateOpen.add(value);
            }
        }

        HashSet<String> storedTimeAndDateClosed = (HashSet<String>) prefrence.getStringSet("TimeAndDateClosed", null);
        value = "";
        if (storedTimeAndDateClosed != null){
            Iterator iterator = storedTimeAndDateClosed.iterator();
            while (iterator.hasNext()) {
                value = (String) iterator.next();
                timeAndDateClosed.add(value);
            }
        }

        // data to populate the RecyclerView with
        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.rvTimeAndDate);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyRecyclerViewAdapter(this, timeAndDateOpen);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);

        RecyclerView recyclerViewClosed = findViewById(R.id.rvTimeAndDateClosed);
        recyclerViewClosed.setLayoutManager(new LinearLayoutManager(this));
        adapterClosed = new MyRecyclerViewAdapter(this, timeAndDateClosed);
        adapterClosed.setClickListener(this);
        recyclerViewClosed.setAdapter(adapterClosed);


    }
    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "You clicked " + adapter.getItem(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
    }
}
