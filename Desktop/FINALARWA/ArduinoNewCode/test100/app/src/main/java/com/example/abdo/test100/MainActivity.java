package com.example.abdo.test100;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends Activity {
    //    private final String DEVICE_NAME="MyBTBee";
    private final String DEVICE_ADDRESS="98:D3:32:21:7C:B1";
    private final UUID PORT_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");//Serial Port Service ID
    public static BluetoothDevice device;
    public static  BluetoothSocket socket;
    private OutputStream outputStream;
    private InputStream inputStream;
    public SharedPreferences prefrence;
    public SharedPreferences.Editor editor;
    Button startButton, openButton,clearButton,stopButton,fingerPrint,closeButton, history,help;
    TextView textView;
    EditText editText;
    boolean deviceConnected=false;
    Thread thread;
    byte buffer[];
    int bufferPosition;
    boolean stopThread;
    private Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        startButton = (Button) findViewById(R.id.buttonStart);
        openButton = (Button) findViewById(R.id.buttonOpen);
        closeButton = (Button) findViewById(R.id.buttonClose);
        clearButton = (Button) findViewById(R.id.buttonClear);
        history = findViewById(R.id.history);
        stopButton = (Button) findViewById(R.id.buttonStop);
        fingerPrint = (Button) findViewById(R.id.setup_fingerprint);
        prefrence = getSharedPreferences("prefence",MODE_PRIVATE);
        help = findViewById(R.id.help);
        editor=prefrence.edit();
        //editText = (EditText) findViewById(R.id.editText);
        textView = (TextView) findViewById(R.id.textView);
        setUiEnabled(false);


        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, History.class));
            }
        });

        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Help.class));
            }
        });

    }



    public void setUiEnabled(boolean bool)
    {
        startButton.setEnabled(!bool);
        openButton.setEnabled(bool);
        closeButton.setEnabled(bool);
        stopButton.setEnabled(bool);
        textView.setEnabled(bool);
        fingerPrint.setEnabled(bool);

    }

    public boolean BTinit()
    {
        boolean found=false;
        BluetoothAdapter bluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(),"Device doesnt Support Bluetooth",Toast.LENGTH_SHORT).show();
        }
        if(!bluetoothAdapter.isEnabled())
        {
            Intent enableAdapter = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableAdapter, 0);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
        if(bondedDevices.isEmpty())
        {
            Toast.makeText(getApplicationContext(),"Please Pair the Device first",Toast.LENGTH_SHORT).show();
        }
        else
        {
            for (BluetoothDevice iterator : bondedDevices)
            {
                if(iterator.getAddress().equals(DEVICE_ADDRESS))
                {
                    device=iterator;
                    found=true;
                    break;
                }
            }
        }
        return found;
    }

    public boolean BTconnect()
    {
        boolean connected=true;
        try {
            socket = device.createRfcommSocketToServiceRecord(PORT_UUID);
            socket.connect();
        } catch (IOException e) {
            e.printStackTrace();
            connected=false;
        }
        if(connected)
        {
            try {
                outputStream=socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                inputStream=socket.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


        return connected;
    }

    public void onClickStart(View view) {
        if(BTinit())
        {
            Log.d("Test", "BeforeConnect");
            if(BTconnect())
            {

                Log.d("Test", "AfterConnect");
                setUiEnabled(true);
                deviceConnected=true;
                beginListenForData();
                textView.append("\nConnection Opened!\n");
            }

        }
    }

    void beginListenForData()
    {
        final Handler handler = new Handler();
        stopThread = false;
        buffer = new byte[1024];
        thread  = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopThread)
                {
                    try
                    {
                        int byteCount = inputStream.available();
                        if(byteCount > 0)
                        {
                            byte[] rawBytes = new byte[byteCount];
                            inputStream.read(rawBytes);
                            final String string=new String(rawBytes,"UTF-8");
                            final HashSet<String> usersID = (HashSet<String>) prefrence.getStringSet("usersID", null);



                            handler.post(new Runnable() {
                                public void run()
                                {
                                    String value = "";
                                    String showOutput = "Admin Fingerprint";
                                    if (usersID != null){
                                        Iterator iterator = usersID.iterator();
                                        while (iterator.hasNext()) {
                                            value = (String) iterator.next();
                                            final String[] userAndId = value.split(",");



                                            if (userAndId[1].equals(string)){
                                                showOutput = value;
                                                new AlertDialog.Builder(mContext)
                                                        .setMessage(userAndId[0] + " is trying to open, do you want to open?")

                                                        // Specifying a listener allows you to take an action before dismissing the dialog.
                                                        // The dialog is automatically dismissed when a dialog button is clicked.
                                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int which) {

                                                                String string = "1";
                                                                try {
                                                                    outputStream.write(string.getBytes());
                                                                } catch (IOException e) {
                                                                    e.printStackTrace();
                                                                }
                                                                dateAndTime("TimeAndDateOpen",userAndId[0]);
                                                                textView.append("\nSent Data:"+string+"\n");

                                                                try {
                                                                    //set time in mili
                                                                  //  Thread.sleep(50000);
                                                                    String string2 = "0";
                                                                    try {
                                                                        outputStream.write(string2.getBytes());
                                                                    } catch (IOException e) {
                                                                        e.printStackTrace();
                                                                    }


                                                                }catch (Exception e){
                                                                    e.printStackTrace();
                                                                }


                                                            }
                                                        })

                                                        // A null listener allows the button to dismiss the dialog and take no further action.
                                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                // Continue with delete operation
                                                                String string = "0";
                                                                try {
                                                                    outputStream.write(string.getBytes());
                                                                } catch (IOException e) {
                                                                    e.printStackTrace();
                                                                }
                                                                dateAndTime("TimeAndDateClosed",userAndId[0]);
                                                                textView.append("\nSent Data:"+string+"\n");

                                                            }
                                                        })
                                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                                        .show();
                                                break;
                                            }
                                        }


                                        if (showOutput.equals("Admin Fingerprint")){

                                            /*try {
                                                outputStream.write("1".getBytes());
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                            dateAndTime("TimeAndDateOpen","Admin Fingrprint");

                                            try {
                                                //set time in mili
                                                Thread.sleep(50000);

                                                try {
                                                    outputStream.write("0".getBytes());
                                                } catch (IOException e) {
                                                    e.printStackTrace();
                                                }


                                            }catch (Exception e){
                                                e.printStackTrace();
                                            }
                                          return;*/
                                            textView.setText(showOutput);
                                        }

                                    }

                                }
                            });

                        }
                    }
                    catch (IOException ex)
                    {
                        stopThread = true;
                    }
                }
            }
        });

        thread.start();
    }

    // on

    @Override
    protected void onPause() {
        super.onPause();
//        if(thread != null)
//            thread.destroy();
    }

    public void onClickOpen(View view) {
        Calendar calender = Calendar.getInstance();
        //Time
        SimpleDateFormat formte=new SimpleDateFormat("HH:mm");
        String time =formte.format(calender.getTime());
        final String[] HM = time.split(":");
        int HmInt=Integer.parseInt(HM[0]);

        //HmInt>=9 && HmInt<=6
        if((HmInt==12) || (HmInt==1) || (HmInt==2) || (HmInt==3) || (HmInt==4) || (HmInt==5) || (HmInt==6) ){


            new AlertDialog.Builder(mContext)
                    .setTitle("WARNING")
                    .setMessage("Are you sure you want to open the door at this time ?")

                    // Specifying a listener allows you to take an action before dismissing the dialog.
                    // The dialog is automatically dismissed when a dialog button is clicked.
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            String string = "1";
                            try {
                                outputStream.write(string.getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            dateAndTime("TimeAndDateOpen","Bluetooth");
                            textView.append("\nSent Data:"+string+"\n");
                        }
                    })

                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Continue with delete operation
                            String string = "0";
                            try {
                                outputStream.write(string.getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            dateAndTime("TimeAndDateClosed","Bluetooth");
                            textView.append("\nSent Data:"+string+"\n");

                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        }

        else{
        String string = "1";
        try {
            outputStream.write(string.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

        dateAndTime("TimeAndDateOpen","Bluetooth");
        textView.append("\nSent Data:"+string+"\n");

    }}

    public void onClickClose(View view) {
        String string = "0";
        try {
            outputStream.write(string.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        dateAndTime("TimeAndDateClosed","Bluetooth");

        textView.append("\nSent Data:"+string+"\n");

    }

    public void onClickFingerprint(View view) {
        startActivity(new Intent(MainActivity.this,  SetupFingerprint.class));

    }

    public void onClickStop(View view) throws IOException {
        stopThread = true;
        outputStream.close();
        inputStream.close();
        socket.close();
        setUiEnabled(false);
        deviceConnected=false;
        textView.append("\nConnection Closed!\n");
    }

    public void dateAndTime(String key ,String Name){
        Calendar calender = Calendar.getInstance();
        //Time
        SimpleDateFormat formte=new SimpleDateFormat("HH:mm:ss");
        String time =formte.format(calender.getTime());
        //Date
        String currentDate= DateFormat.getDateInstance().format(calender.getTime());
        //Hashset time
        HashSet<String> timeHash = (HashSet<String>) prefrence.getStringSet(key, null);
        if(timeHash == null) {
            timeHash = new HashSet<>();
        }
        if(key.equals("TimeAndDateOpen"))
            timeHash.add("Opened at :" + time + " Date: "+ currentDate +" By " + Name);
        else{
            timeHash.add("Closed at :" + time + " Date: "+ currentDate  +" By " + Name);
        }

        SharedPreferences.Editor editor = prefrence.edit();
        editor.putStringSet(key, timeHash).apply();
    }




    public void onClickClear(View view) {
        textView.setText("");
    }
}
