package com.example.abdo.test100;

import android.app.ActionBar;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.UUID;

public class SetupFingerprint extends AppCompatActivity {
    private OutputStream outputStream;
    private InputStream inputStream;
    Thread thread;
    byte buffer[];
    int bufferPosition;
    boolean stopThread;
    Button done;
   public EditText first_user,second_user,third_user,fourth_user,fifth_user;

    public SharedPreferences prefrence;
    public SharedPreferences.Editor editor;
    private final UUID PORT_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");//Serial Port Service ID
    //private BluetoothSocket socket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_fingerprint);
       first_user=(EditText) findViewById(R.id.first_user);
        second_user=(EditText) findViewById(R.id.second_user);
        third_user=(EditText) findViewById(R.id.third_user);
        fourth_user=(EditText) findViewById(R.id.fourth_user);
        fifth_user=(EditText) findViewById(R.id.fifth_user);
        prefrence = getSharedPreferences("prefence",MODE_PRIVATE);
        editor=prefrence.edit();
        try {
            inputStream=MainActivity.socket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    public boolean BTconnect()
//    {
//        boolean connected=true;
//        try {
//            socket = MainActivity.device.createRfcommSocketToServiceRecord(PORT_UUID);
//            socket.connect();
//        } catch (IOException e) {
//            e.printStackTrace();
//            connected=false;
//        }
//        if(connected)
//        {
//            try {
//                outputStream=socket.getOutputStream();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//
//        }
//
//
//        return connected;
//    }

    public void OnClickUsers(View view){
        String n = view.getTag().toString();

        switch (n){
            case "1":
                beginListenForData(first_user);
                break;
            case "2":
                beginListenForData(second_user);
                break;
            case "3":
                beginListenForData(third_user);
                break;
            case "4":
                beginListenForData(fourth_user);
                break;
            case "5":
                beginListenForData(fifth_user);
                break;
                default:
                    Log.d("SetupFingerPrint", "eror");

        }

    }

    void beginListenForData(final EditText userEditText)
    {
        final Handler handler = new Handler();
        stopThread = false;
        buffer = new byte[1024];
        Thread thread  = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopThread)
                {
                    try
                    {
                        int byteCount = inputStream.available();
                        if(byteCount > 0)
                        {
                            byte[] rawBytes = new byte[byteCount];
                            inputStream.read(rawBytes);
                            final String string=new String(rawBytes,"UTF-8");

                            handler.post(new Runnable() {
                                public void run()
                                {
//
                                    HashSet<String> usersId = (HashSet<String>) prefrence.getStringSet("usersID", null);
                                    if(usersId == null) {
                                        usersId = new HashSet<>();
                                    }
                                    usersId.add(userEditText.getText().toString() + "," + string);
                                    SharedPreferences.Editor editor = prefrence.edit();
                                    editor.putStringSet("usersID", usersId).apply();

                                 //   Num=  prefrence.getString("number", String.valueOf(number));


                                }
                            });

                        }
                    }
                    catch (IOException ex)
                    {
                        stopThread = true;
                    }
                }
            }
        });

        thread.start();
    }
}
